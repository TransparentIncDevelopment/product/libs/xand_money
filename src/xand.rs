mod arith;
use crate::{Money, MoneyError, Usd};

use crate::Decimal;
#[cfg(test)]
mod unit_tests;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Xand(Decimal);

// Implement currency-specific methods
impl Xand {
    pub fn from_decimal(decimal: Decimal) -> Result<Self, MoneyError> {
        match decimal > Decimal::new(0, 0) {
            true => Ok(Self(decimal)),
            false => Err(MoneyError::CannotCreateNonPositiveXandAmounts(decimal)),
        }
    }

    pub fn from_usd(usd: Usd) -> Result<Self, MoneyError> {
        match usd > Usd::from_i64_minor_units(0)? {
            true => Ok(Self(usd.into_major_units())),
            false => Err(MoneyError::CannotCreateNonPositiveXandAmounts(
                usd.into_major_units(),
            )),
        }
    }

    #[must_use]
    #[inline]
    pub const fn amount(self) -> Decimal {
        self.0
    }
}
