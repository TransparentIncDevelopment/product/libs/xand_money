#![allow(clippy::inconsistent_digit_grouping)]
use super::*;
use std::f64;

#[test]
fn from_str__valid_str_succeeds() {
    let sut = Usd::from_str("2.34");
    assert!(sut.is_ok());
}

#[test]
fn from_str__invalid_str_fails() {
    // TODO - consider adding proptest for random from_str variations
    // given
    let string_with_special_char = "$1.23";

    // when
    let res = Usd::from_str(string_with_special_char);

    //then
    assert!(res.is_err());
}

#[test]
fn from_str__valid_minor_unit_amt_constructed() {
    let usd = Usd::from_str("9999.00").unwrap();
    let expected_amt_minor_units = Decimal::new(9999, 0);

    assert_eq!(usd.into_major_units(), expected_amt_minor_units);
}

#[test]
fn from_str__valid_major_unit_amt_constructed() {
    let usd = Usd::from_str("4999.99").unwrap();
    let expected_amt_major_units = Decimal::new(499_999, 2);

    assert_eq!(usd.into_major_units(), expected_amt_major_units);
}

#[test]
fn from_i64__valid_minor_unit_amt_constructed() {
    let usd = Usd::from_i64_minor_units(9925).unwrap();
    let expected_amt_minor_units = Decimal::new(99_25, 2);

    assert_eq!(usd.into_major_units(), expected_amt_minor_units);
}

#[test]
fn from_i64__valid_major_unit_amt_constructed() {
    let usd = Usd::from_i64_minor_units(499).unwrap();
    let expected_amt_major_units = Decimal::new(499, 2);

    assert_eq!(usd.into_major_units(), expected_amt_major_units);
}

#[test]
fn from_f64__valid_amount_succeeds() {
    let sut = Usd::from_f64_major_units(123.45);
    assert!(sut.is_ok());
}

#[test]
fn from_f64__max_invalid_amount_fails() {
    let res = Usd::from_f64_major_units(f64::MAX);
    assert!(res.is_err());
}

#[test]
fn from_f64_fractional_cents_fails() {
    let res = Usd::from_f64_major_units(100.001);
    assert!(res.is_err());
}
