#![allow(clippy::inconsistent_digit_grouping)]
use super::*;

#[test]
fn from_decimal__constructs_xand_correctly() {
    let xand = Xand::from_decimal(Decimal::new(123_999, 2));
    assert!(xand.is_ok());
}
#[test]
fn from_usd__constructs_xand_correctly() {
    let xand1 = Xand::from_usd(Usd::from_i64_minor_units(123_49).unwrap());
    let xand2 = Xand::from_usd(Usd::from_f64_major_units(123.49).unwrap());
    assert!(xand1.is_ok());
    assert!(xand2.is_ok());
    assert_eq!(xand1.unwrap(), xand2.unwrap());
}
