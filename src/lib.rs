#![warn(clippy::all, clippy::nursery, clippy::pedantic, rust_2018_idioms)]
// TODO - To enable below restriction, add Dyn to trait objects
//#![forbid(bare_trait_objects)]
// To use the `unsafe` keyword, change to `#![allow(unsafe_code)]` (do not remove); aids auditing.
#![forbid(unsafe_code)]
// Safety-critical application lints
#![deny(
    clippy::pedantic,
    clippy::float_cmp_const,
    clippy::indexing_slicing,
    clippy::integer_arithmetic,
    clippy::unwrap_used
)]
#![allow(
    clippy::match_bool,
    clippy::integer_arithmetic,
    clippy::missing_errors_doc
)]
// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing license files and more
//#![warn(clippy::cargo, clippy::restriction, missing_docs, warnings)]
//#![deny(warnings)]

pub mod consts;
mod money_error;
mod usd;
mod xand;

pub use rust_decimal::Decimal;
pub use {money_error::MoneyError, usd::Usd, xand::Xand};

use rust_decimal::prelude::ToPrimitive;

pub type Result<T, E = MoneyError> = std::result::Result<T, E>;

/// This trait can be implemented as different currencies, providing a common interface to work with
/// fiat money.
///
/// ## Invariants:
/// 1. You must not allow your type which implements this trait to be instantiated with a value
///    which is not representable in an integer amount of the currency's minor unit. IE: You cannot
///    represent less than a cent in USD. Violating this invariant will result in panics.
/// 2. Values (in major units) that would exceed `MAX_ABS_CONTIGUOUS_F64_USD` with either sign are
///    not permitted.
/// 3. Values (in minor units) that would exceed `MAX_ABS_D96_DOLLARS_CONVERTIBLE_TO_MINOR_UNITS`
///    with either sign are not permitted.
pub trait Money: Copy {
    type Error: std::error::Error;

    /// Given a string representing an amount of this currency in major units, attempt to parse it
    /// and return `Self`.
    ///
    /// Fails if the amount provided would violate invariant 1.
    fn from_str<S: AsRef<str>>(s: S) -> Result<Self, Self::Error>;

    /// Create `Self` from a float representing an amount of this currency in major units.
    ///
    /// Fails if the amount provided would violate invariant 1.
    fn from_f64_major_units<N: Into<f64>>(val: N) -> Result<Self, Self::Error>;

    /// Create `Self` from an integer representing an amount of this currency in minor units.
    ///
    /// Fails if the amount provided would violate any invariants.
    fn from_i64_minor_units<N: Into<i64>>(val: N) -> Result<Self, Self::Error>;

    /// Create `Self` from an unsigned integer representing an amount of this currency in minor units.
    ///
    /// Fails if the amount provided would violate any invariants.
    fn from_u64_minor_units<N: Into<u64>>(val: N) -> Result<Self, Self::Error>;

    /// Create `Self` from a XAND money instance - performing conversion as necessary
    fn from_xand(xand: Xand) -> Self;

    /// Returns a `Decimal` instance representing the currency in it's minor unit.
    ///
    /// EX: $1.01 would be returned as a `Decimal` with value `101`
    fn into_minor_units(self) -> Decimal;

    /// Returns a `Decimal` instance representing the currency in it's major unit.
    ///
    /// EX: $1.01 would be returned as a `Decimal` with value `1.01`
    fn into_major_units(self) -> Decimal;

    /// Returns a `u64` representing the currency in its minor unit.
    ///
    /// EX: $1.01 would be returned as `101`
    ///
    /// # Errors:
    /// * If the value inside this instance is not positive
    ///
    /// # Panics:
    /// * If this `Money` implementation violated invariant 1 when constructing itself
    fn into_u64_minor_units(self) -> Result<u64, MoneyError> {
        let minor = self.into_minor_units();
        if minor.is_sign_negative() {
            return Err(MoneyError::ValueIsNegative(minor));
        }

        Ok(minor.to_u64().expect(
            "Conversion to u64 is infallible because Money implementations \
             cannot be constructed with finer-than-minor-unit granularity",
        ))
    }

    /// Returns an `i64` representing the currency in its minor unit.
    ///
    /// EX: $1.01 would be returned as `101`
    ///
    /// # Errors:
    /// * If the value inside this instance cannot be represented as an i64
    fn into_i64_minor_units(self) -> Result<i64, MoneyError> {
        let minor = self.into_minor_units();
        let i64_representation_result = minor.to_i64();
        i64_representation_result.map_or_else(
            || {
                Err(MoneyError::MinorUnitRepresentationFailure {
                    value: minor,
                    target_type_name: std::any::type_name::<i64>().to_string(),
                })
            },
            Ok,
        )
    }
}
