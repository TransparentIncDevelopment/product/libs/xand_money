use crate::{
    consts::{MAX_ABS_CONTIGUOUS_F64_USD, MAX_ABS_D96_DOLLARS_CONVERTIBLE_TO_MINOR_UNITS},
    Decimal, Money, MoneyError, Xand,
};
use rust_decimal::prelude::{FromPrimitive, ToPrimitive};
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use std::str::FromStr;

#[cfg(test)]
mod unit_tests;

/// A representation of USD implementing `Money`
#[derive(
    Clone, Copy, Debug, Deserialize, Default, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize,
)]
pub struct Usd(Decimal);

impl Usd {
    /// Parent validator for type constructors
    fn validate(val: Decimal) -> Result<Decimal, <Self as Money>::Error> {
        Self::verify_convertible_to_minor_units(val).and_then(Self::verify_integral_cents)
    }

    /// Verify can convert input amount into minor units not exceeding type capacity
    fn verify_convertible_to_minor_units(v: Decimal) -> Result<Decimal, <Self as Money>::Error> {
        match v >= -MAX_ABS_D96_DOLLARS_CONVERTIBLE_TO_MINOR_UNITS
            && v <= MAX_ABS_D96_DOLLARS_CONVERTIBLE_TO_MINOR_UNITS
        {
            true => Ok(v),
            false => Err(MoneyError::ValueTooLargeToConvertToMinorUnits(v)),
        }
    }

    /// Verify input amount does not contain fractional cents
    fn verify_integral_cents(v: Decimal) -> Result<Decimal, <Self as Money>::Error> {
        match (v * Decimal::new(100, 0)).fract() == Decimal::new(0, 0) {
            true => Ok(v),
            false => Err(MoneyError::ValueContainsFractionalMinorUnits(v)),
        }
    }

    /// Create a verified major Decimal value
    fn create_verified_major_value_from_minor(
        minor_value: Decimal,
    ) -> Result<Decimal, <Self as Money>::Error> {
        let major_value = minor_value / Decimal::new(100, 0);
        Self::validate(major_value)
    }
}

impl Money for Usd {
    type Error = MoneyError;

    fn from_str<S: AsRef<str>>(s: S) -> Result<Self, Self::Error> {
        let value = Decimal::from_str(s.as_ref())?;
        let verified_value = Self::validate(value)?;
        Ok(Self(verified_value))
    }

    fn from_f64_major_units<N: Into<f64>>(val: N) -> Result<Self, Self::Error> {
        let value = Decimal::from_f64(val.into()).ok_or(MoneyError::ErrorConstructingDecimal)?;
        let verified_value = Self::validate(value)?;
        Ok(Self(verified_value))
    }

    fn from_i64_minor_units<N: Into<i64>>(val: N) -> Result<Self, Self::Error> {
        let value = Decimal::from_i64(val.into()).expect("Decimal from i64 always works");
        let verified_value = Self::create_verified_major_value_from_minor(value)?;
        Ok(Self(verified_value))
    }

    fn from_u64_minor_units<N: Into<u64>>(val: N) -> Result<Self, Self::Error> {
        let value = Decimal::from_u64(val.into()).expect("Decimal from u64 always works");
        let verified_value = Self::create_verified_major_value_from_minor(value)?;
        Ok(Self(verified_value))
    }

    fn from_xand(xand: Xand) -> Self {
        Self(xand.amount())
    }

    fn into_minor_units(self) -> Decimal {
        self.0 * Decimal::new(100, 0)
    }

    fn into_major_units(self) -> Decimal {
        self.0
    }
}

// Silence (strict) `clippy::nursery` lint arising from `unreachable!()` expanding to `f64`
// instead of (idiomatic) `Self`.
/// Impl `try_from()` to convert Usd to f64
#[allow(clippy::use_self)]
impl TryFrom<Usd> for f64 {
    type Error = MoneyError;

    fn try_from(usd: Usd) -> Result<Self, Self::Error> {
        match usd
            >= Usd::from_f64_major_units(-MAX_ABS_CONTIGUOUS_F64_USD)
                .unwrap_or_else(|e| unreachable!("{}", e))
            && usd
                <= Usd::from_f64_major_units(MAX_ABS_CONTIGUOUS_F64_USD)
                    .unwrap_or_else(|e| unreachable!("{}", e))
        {
            true => Ok(usd
                .into_major_units()
                .to_f64()
                .ok_or_else(|| MoneyError::ErrorConstructingF64CausedBy(usd.into_major_units()))?),
            false => Err(
                MoneyError::ValueTooLargeToConvertToF64WithoutPrecisionErrors(
                    usd.into_major_units(),
                ),
            ),
        }
    }
}
